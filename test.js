var Client = require('node-rest-client').Client;
var Q = require('q');
var request = require('superagent');
var EventEmitter = require('events').EventEmitter;
var event = new EventEmitter();
var moment = require("moment");
var startDateExecution;
var parsePath = require('parse-filepath');
var fs = require('fs');
var isDelete = false;
var pathD = require('path');
var gitlab = require('node-gitlab');
require("moment-duration-format");

// configure proxy
var options_proxy = {
    proxy: {
        host: "",
        port: 8080,
        tunnel: true
    },

};

/*mimetypes: {
 json: ["application/json", "application/json;charset=utf-8"]
 }*/
var TOKEN_SOURCE = "";
var TOKEN_TARGET = "";
var ID_GROUP_TARGET = "";
var ID_PROJECT_TARGET = 0;
var ID_PROJECT_SOURCE = 0;

var HAS_PROXY = false;

var URL_SOURCE = '';
var URL_SOURCE_IMAGE = '';
var URL_TARGET = 'https://gitlab.com/api/v4/';


var HEADERS = {
    "PRIVATE-TOKEN": TOKEN_TARGET
};
var HEADERS_NABLA = {
    "PRIVATE-TOKEN": TOKEN_SOURCE
};
var patternPathFile = /\((\/[\w|\/|\.|\-]+)\)/gim;
var patternMarkdownFile = /(!\[[\w]+\]\(\/[\w|\/|\.|\-]+\))/gim;


client.milestones.list({id: 1})
    .then(function (milestones) {
        console.log(milestones);
    })
    .catch(function (err) {
        throw err;
    });


if (HAS_PROXY) {
    var client = new Client(options_proxy);
    var clientSource = gitlab.createPromise({
        api: URL_SOURCE,
        privateToken: TOKEN_SOURCE,
    });
} else {
    var client = new Client();
}


var finalUrlTarget = URL_TARGET + "/projects/" + ID_PROJECT_TARGET;
var finalUrlSource = URL_SOURCE + "/projects/" + ID_PROJECT_SOURCE;


//var issues = [];

function getIssues(token, url, indexPage) {
    var defer = Q.defer();
    var issues = [];
    if (typeof indexPage != 'undefined' && typeof url != 'undefined' && typeof indexPage === 'number') {
        var finalUrl = url + "/issues";//?&&sort=desc&per_page=100&page=" + indexPage;
        request
            .get(finalUrl)
            .set('PRIVATE-TOKEN', token)
            .query({state: 'opened'})
            .query({order_by: 'created_at'})
            .query({sort: 'desc'})
            .query({per_page: '3'})
            .query({page: 1})
            .end(function (err, res) {
                if (err) {
                    console.log("Error " + res.body.message + ", status code :" + res.status);
                    defer.reject("Error " + res.body.message + ", status code :" + res.status);
                } else {
                    if (res.body.length != 0) {
                        console.log(res.body.length + " issue found for page " + indexPage);
                        issues = issues.concat(res.body);
                        issues.reverse();
                        defer.resolve(issues);
                        /* getIssues(token, url, indexPage + 1).then(function (dataIssues) {
                         issues = issues.concat(dataIssues);
                         defer.resolve(issues);
                         });*/
                    } else {
                        console.log(res.body.length + " issue found for page " + indexPage);
                        defer.resolve(issues);
                    }
                }
            });

        /* client.get(finalUrl, args, function (data, response) {
         if (response.statusCode == 200) {
         if (data.length != 0) {
         console.log(data.length + " issue found for page " + indexPage);
         issues = issues.concat(data);
         getIssues(args, url, indexPage + 1).then(function (dataIssues) {
         issues = issues.concat(dataIssues);
         defer.resolve(issues);
         });
         } else {
         console.log(data.length + " issue found for page " + indexPage);
         defer.resolve(issues);
         }
         } else {
         defer.reject("Error call, status code :" + response.statusCode);
         }
         });*/
    } else {
        defer.reject("Bad parameter");
    }

    return defer.promise;
}
var URL_POST = finalUrlGitlab + "/issues";

function sendIssues(token, url, issue) {
    var defer = Q.defer();
    request
        .post(url)
        .send(issue)
        .set('PRIVATE-TOKEN', token)
        .end(function (err, res) {
            if (err) {
                console.log("Error " + res.body.message + ", status code :" + res.status);
                defer.reject("Error " + res.body.message + ", status code :" + res.status);
            } else {
                defer.resolve(res.body);
            }
        });
    return defer.promise;
}
function deleteIssue(token, url, issueIID) {
    var deleteUrl = url + "/" + issueIID;
    var defer = Q.defer();
    request
        .delete(deleteUrl)
        .set('PRIVATE-TOKEN', token)
        .end(function (err, res) {
            if (err) {
                console.log("Error " + res.body.message + ", status code :" + res.status);
                defer.reject("Error " + res.body.message + ", status code :" + res.status);
            } else {
                defer.resolve(res.body);
            }
        });
    return defer.promise;
}

var args = {
    headers: HEADERS
};
var args_nabla = {
    headers: HEADERS_NABLA
};


event.on('start-syncro', function (issues) {
    console.log("start syncro");
    event.on('next', function () {
        if (issues.length > 0) {
            var current = issues.pop();
            console.log("next issue " + current.iid);
            event.emit('send', current);
        } else {
            console.log("Finished syncro");
            var end = new Date() - startDateExecution;
            var result = moment.duration(end, "ms").format("h [h] m [mins] s [sec] S [ms]");
            console.log("Total duration : " + result);
        }

    });
    event.emit('next');
});
event.on('start-deleting', function (issues) {
    console.log("start deleting");
    event.on('next-delete', function () {
        if (issues.length > 0) {
            var current = issues.pop();
            console.log("delete issue id = " + current.iid);
            event.emit('delete', current);
        } else {
            console.log("Finished deleting");
            var end = new Date() - startDateExecution;
            var result = moment.duration(end, "ms").format("h [h] m [mins] s [sec] S [ms]");
            console.log("Total duration : " + result);

        }

    });
    event.emit('next-delete');
});
event.on('delete', function (issue) {
    deleteIssue(TOKEN_TARGET, URL_POST, issue.iid).then(function () {
        console.log("Issue is deleted, id = " + issue.iid);
        event.emit('next-delete');
    }, function (errorSend) {
        console.log(errorSend);
    });
});
event.on('send', function (issue) {
    var tmpIssue = {
        title: issue.title,
        description: issue.description,
        created_at: issue.created_at

    };

    var resultExtractPathFiles = patternPathFile.exec(issue.description);
    if (resultExtractPathFiles != null && resultExtractPathFiles.length > 1) {
        var pathAttachment = resultExtractPathFiles[1];
        var finalPathAttachement = URL_SOURCE_IMAGE + pathAttachment;
        var infoPath = parsePath(pathAttachment);
        var tmpfile = pathD.join(__dirname + "/tmp/", infoPath.basename);
        const stream = fs.createWriteStream(tmpfile);

        //STEP 2
        //flux d'écriture
        //losque le flux est fini, on upload le fichier sur l'autre serveur.
        stream.on('finish', function () {
            console.log("finish write");
            request
                .post(finalUrlGitlab + "/uploads")
                .set('PRIVATE-TOKEN', TOKEN_TARGET)
                .timeout({
                    response: 5000,  // Wait 5 seconds for the server to start sending,
                    deadline: 300000, // but allow 5 minute for the file to finish loading.
                })
                .attach('file', tmpfile)
                .end(function (err, res) {
                    if (err) {
                        console.log("Error " + res.error + ", status code :" + res.status);
                        //defer.reject("Error " + res.body.message + ", status code :" + res.status);
                    } else {
                        console.log("Attachment for issue " + issue.iid + " is uploaded");

                        if (tmpIssue.description.length > 0) {
                            var newDescription = tmpIssue.description.replace(patternMarkdownFile, res.body.markdown);
                            tmpIssue.description = newDescription;
                        }

                        sendIssues(TOKEN_TARGET, URL_POST, tmpIssue).then(function (ret) {
                            console.log("Issue " + issue.iid + " is created with id " + ret.iid);
                            event.emit('next');
                        }, function (errorSend) {
                            console.log(errorSend);
                        });
                    }
                });
        });

        //STEP 1
        //On récupère le fichier attaché puis on l'écrit sur le disque
        request
            .get(finalPathAttachement)
            .set('PRIVATE-TOKEN', TOKEN_SOURCE)
            .timeout({
                response: 5000,  // Wait 5 seconds for the server to start sending,
                deadline: 300000, // but allow 1 minute for the file to finish loading.
            })
            .pipe(stream);

    } else {
        sendIssues(TOKEN_TARGET, URL_POST, tmpIssue).then(function (ret) {
            console.log("Issue " + issue.iid + " is created with id " + ret.iid);
            event.emit('next');
        }, function (errorSend) {
            console.log(errorSend);
        });
    }
});

if (isDelete) {

    //Suppression
    startDateExecution = new Date();
    getIssues(TOKEN_TARGET, finalUrlGitlab, 1).then(function (gitIssues) {
        if (gitIssues.length != 0) {

            event.emit('start-deleting', gitIssues);
        }
    }, function (err) {
        console.log("Error call target gitlab :" + err);
    });

} else {
// Syncro
    startDateExecution = new Date();
    getIssues(TOKEN_TARGET, finalUrlGitlab, 1).then(function (gitIssues) {
        getIssues(TOKEN_SOURCE, finalUrlNabla, 1).then(function (nablaIssues) {
            if (gitIssues.length == 0) {
                event.emit('start-syncro', nablaIssues);
            } else {

            }
        }, function (errNabla) {
            console.log("Error call source gitlab :" + errNabla);
        });
    }, function (err) {
        console.log("Error call target gitlab :" + err);
    });
}


/*
 console.log("No data for page " + indexPage);
 console.log("Download issues finished");
 sendIssues(issues);
 } else {*/